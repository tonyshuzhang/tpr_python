# Import system modules
import arcpy
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "C:/sapyexamples/data"

# Set local variables
inDestination = "observers.shp"
costRaster = "costraster"
backLink = "backlink2"
method = "EACH_CELL"
destField = "FID"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Execute CostPath
outCostPath = CostPath(inDestination, costRaster, backLink, method,
                       destField)

# Save the output
outCostPath.save("c:/sapyexamples/output/costpath02")
